﻿Function Test-ADAuthentication {
    param($username,$password)
    (new-object directoryservices.directoryentry "",$username,$password).psbase.name -ne $null
}

$user = Read-Host -Prompt 'Domain\Username'
$pass = Read-Host -Prompt 'password' -AsSecureString
$bstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($pass)
$Value = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstr)

Test-ADAuthentication "$($user)" "$($value)"
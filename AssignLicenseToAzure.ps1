﻿  $ErrorActionPreference = "Stop"
  Connect-MSOLService
  $users = import-csv 'c:\temp\users.csv' -delimiter "," 
  foreach ($user in $users) 
    { 
        $upn=$user.UPN
        $usagelocation=$user.usagelocation 
        $SKU=$user.SKU
        $license = (Get-MsolUser -UserPrincipalName $upn).licenses.accountskuid
        $opts = New-MsolLicenseOptions -AccountSkuId $SKU –DisabledPlans SWAY
        $serviceStatus = (Get-MsolUser -UserPrincipalName $upn).licenses.servicestatus

        if ($license)
                
                {
                    Write-Host "            "
                    Write-Host "User $($UPN) already has a $($license) License assigned, disabling all services except Office 365 Pro Plus" -ForegroundColor Yellow
                    Set-MsolUserLicense -UserPrincipalName $upn -LicenseOptions $opts
                    Write-Host "            "
                    Write-Host "List of enabled and disable services under $($license)"
                    $serviceStatus
                }
        
        Else
                {
                    
                    Write-Host "Assigning licenses to following users: $($upn)" -foregroundcolor yellow
                    Set-MsolUser -UserPrincipalName $upn -UsageLocation $usagelocation
                    Write-Host "Location is now set to AU" 
                    Set-MsolUserLicense -UserPrincipalName $upn -AddLicenses $SKU -LicenseOptions $opts
                    Write-Host "License is now added for the user"
                    Write-Host "User now has following license active for their UPN"
                    Write-Host "List of enabled and disable services under Enterprise license"
                    $serviceStatus
                
               }

               #Store the information from this run into the array
               [PSCustomObject]@{
                                   UPN = $UPN
                                   License = $license 
                                   Services = $servicestatus

                                } | Export-csv "c:\temp\test.csv" -notype -Append
        
    }

   #Test Repo
